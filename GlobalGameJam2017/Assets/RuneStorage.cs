﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneStorage : MonoBehaviour {

    public GameObject[] runes;
    EmissionSine[] sine;
    AudioSource audio;

	// Use this for initialization
	void Start ()
    {
        audio = GetComponent<AudioSource>();
        sine = GetComponentsInChildren<EmissionSine>();
        for (int i = 0; i < runes.Length; i++)
        {
            sine[i].disable = true;
        }
    }

    public void RunesOn()
    {
        for (int i = 0; i < sine.Length; i++)
        {
            sine[i].disable = false;
            audio.Play();
        }
    }

}
