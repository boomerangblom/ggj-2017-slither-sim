﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    public float cameraSpeed;
    public float cameraXSpeed;
    public GameObject snake;
    public float trailBehind;
    [Tooltip("Uses the trigger to start the camera follow / Make Sure to enable it in the Heirarchy")]
    public bool startCamFollow;
    [Tooltip("Set timer to zero if using other method")]
    public float timer;

    public float minSpeed;
    public float closeToCamBeforeSlowdown;

    public bool NeedSnake = true;

    //[HideInInspector]
    public GameObject music;

    private SnakeHead snakeHead;
    private float internCamSpeed;

    // Use this for initialization
    void Start()
    {
        snakeHead = snake.GetComponent<SnakeHead>();
        internCamSpeed = cameraSpeed;
        music = GameObject.FindGameObjectWithTag("Music");
    }

    // Update is called once per frame
    void Update()
    {
        if (timer >= 0.0f)
        {
            timer -= Time.deltaTime;
        }

        if(NeedSnake)
        {
            if (startCamFollow && snakeHead.hitCollider)
            {
                startCamFollow = false;
            }
        }

        if (!startCamFollow && timer <= 0.0f)
        {
            if(NeedSnake)
            {
                if (snake.transform.position.z <= transform.position.z - trailBehind - closeToCamBeforeSlowdown)
                {
                    internCamSpeed = Mathf.Lerp(internCamSpeed, minSpeed, Time.deltaTime);

                    if (internCamSpeed <= minSpeed)
                    {
                        internCamSpeed = minSpeed;
                    }
                }
                else
                {
                    internCamSpeed = cameraSpeed;
                }
                if (snake.transform.position.z >= transform.position.z - trailBehind)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, (Mathf.Lerp(transform.position.z, snake.transform.position.z + trailBehind, Time.deltaTime)));
                }
                transform.position = new Vector3(Mathf.Lerp(transform.position.x, snake.transform.position.x, Time.deltaTime * cameraXSpeed), transform.position.y, transform.position.z);
                transform.position += new Vector3(0, 0, Time.deltaTime * internCamSpeed);
            }
           else
            {
                transform.position += new Vector3(0, 0, Time.deltaTime * internCamSpeed);

                if(Input.GetKeyDown(KeyCode.P))
                {
                    SceneManager.LoadScene(1);
                    DontDestroyOnLoad(music);

                }
            }
        }
    }
}
