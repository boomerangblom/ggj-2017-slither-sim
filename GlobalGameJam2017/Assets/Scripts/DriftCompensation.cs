﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriftCompensation : MonoBehaviour {
    
    [Range(0.0f, 1.0f)]
    public float driftCompensation = 0.95f;

    private Rigidbody bodyPhysics;

	// Use this for initialization
	void Start ()
    {
        bodyPhysics = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        //drift compensation
        bodyPhysics.velocity -= transform.right * (Vector3.Dot(bodyPhysics.velocity, transform.right) * driftCompensation);
	}
}
