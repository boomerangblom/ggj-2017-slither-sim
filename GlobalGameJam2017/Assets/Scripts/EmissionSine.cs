﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmissionSine : MonoBehaviour {

    public bool disable = false;

    public float sineValue;
    public float sineSpeed = 1.0f;
    public float minSine = 0.3f;
    public float randomness = 0.5f;

    public float timerStart = 0.0f;

    private float timer;
    private float sineMulti;

    private Renderer renderer;
    private Color baseColor;
    private Material mat;

    public float alphaLevel = 0.5f;

    // Use this for initialization
    void Start ()
    {
        sineMulti = 1 - minSine + 0.1f;
        timer = timerStart;
        sineSpeed += Random.Range(-randomness, randomness);

        renderer = GetComponent<Renderer>();
        mat = renderer.material;

        baseColor = mat.GetColor("_EmissionColor");
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!disable)
        {
            timer += Time.deltaTime;
            sineValue = ((Mathf.Sin(timer * sineSpeed) * 0.5f) + 0.5f) * sineMulti + minSine;
            mat.SetColor("_EmissionColor", baseColor * sineValue);
        }
        else
        {
            mat.SetColor("_EmissionColor", baseColor * 0);
        }
	}

    public void SetAlpha()
    {
        Color color = mat.color;
        color.a = alphaLevel;
        mat.SetColor("_Color", color);
    }
}
