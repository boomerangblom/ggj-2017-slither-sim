﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameControllerScript : MonoBehaviour {

    private Vector3 startPos;

    public Object snakePrefab;
    public GameObject fullSnakeObj;

    private GameObject snake;
    public GameObject cameraObj;
    public bool death;

    public GameObject UI;
    public GameObject scoreText;
    public Text score;
    public Text lostText;

    public GameObject hardButton;
    
    private Image buttonBG;
    private bool hardmode;
    private CameraMove cameraMove;
    private SnakeHead snakeHead;

    public GameObject music;

	// Use this for initialization
	void Start () {
        if(GameObject.FindGameObjectWithTag("Music") == null)
        {
            //Instantiate(music);
        }

        snake = fullSnakeObj.transform.Find("Head").gameObject;

        hardButton.SetActive(true);
        hardmode = true;
        UI.SetActive(false);

        startPos = snake.transform.position;
        lostText.text = "You Lost!";

        SnakeHead.GameWon += GameIsWon;
        SnakeHead.GameStarted += SetButtonOff;
        
        buttonBG = hardButton.GetComponent<Image>();
        cameraMove = cameraObj.GetComponent<CameraMove>();
        snakeHead = snake.GetComponent<SnakeHead>();

        cameraMove.timer = 0;
        cameraMove.startCamFollow = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (death)
        {
            if (snake.transform.position.z - 5.0f <= cameraObj.transform.position.z)
            {
                snakeHead.disableControl = true;
                UI.SetActive(true);
                if (!hardmode)
                {
                    scoreText.SetActive(false);
                }
                else
                {
                    int distance = (int)snake.transform.position.z - (int)startPos.z;
                    score.text = distance.ToString() + "m";
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (!hardmode)
            {
                cameraMove.timer = 3.0f;
                cameraMove.startCamFollow = false;

                cameraObj.transform.position = snakeHead.lastCamPoint;

                EmissionSine[] sines;
                sines = fullSnakeObj.GetComponentsInChildren<EmissionSine>();

                for (int i = 0; i < sines.Length; i++)
                {
                    sines[i].SetAlpha();
                    sines[i].disable = true;
                }

                fullSnakeObj = (GameObject)Instantiate(snakePrefab);
                snake = fullSnakeObj.transform.Find("Head").gameObject;

                fullSnakeObj.transform.position = snakeHead.lastCheckpoint;


                snakeHead.SpawnAtLastCheckpoint();

                cameraMove.snake = snake;

                snakeHead = snake.GetComponent<SnakeHead>();
                UI.SetActive(false);
                scoreText.SetActive(true);
            }
            else
            {
                SnakeHead.GameWon -= GameIsWon;
                SnakeHead.GameStarted -= SetButtonOff;
                UI.SetActive(false);
                SceneManager.LoadScene(1);
            }
            snakeHead.disableControl = false;
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            SnakeHead.GameWon -= GameIsWon;
            SnakeHead.GameStarted -= SetButtonOff;
            UI.SetActive(false);
            SceneManager.LoadScene(1);
        }
    }

    void GameIsWon(bool won)
    {
        if(won)
        {
            cameraMove.cameraSpeed = 0;
            UI.SetActive(true);
            lostText.text = "You Won!";
            int distance = (int)snake.transform.position.z - (int)startPos.z;
            score.text = distance.ToString() + "m";
            cameraObj.GetComponent<CameraMove>().startCamFollow = true;
        }
    }

    public void HardMode()
    {
        if(hardmode)
        {
            hardmode = false;
            buttonBG.color = Color.white;
        }
        else
        {
            hardmode = true;
            buttonBG.color = Color.red;
        }
    }

    void SetButtonOff(bool on)
    {
        hardButton.SetActive(!on);
    }
}
