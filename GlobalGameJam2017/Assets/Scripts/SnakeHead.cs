﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeHead : MonoBehaviour
{
    GameObject cameraObject;

    [HideInInspector]
    public bool hitCollider = false;

    public bool disableControl = false;

    //Publicly editable values
    public float acceleration;
    public float deceleration;

    public float accelerationOnJump;
    public float decelerationWhileSliding;

    public float rotation;
    public float rotationWhileSliding;
    public float maxVelocity = 5.0f;

    public float straightAngle = 40.0f;
    public float fullStopAngle = 70.0f;

    public float angleStiffness = 0.2f;

    public KeyCode jump;
    public Vector3 jumpVelocity;
    public float dragInAir = 0.01f;

    public Vector3 AxisOfRotation;
    public Rigidbody lastTail;
    public Rigidbody tailMiddle;

    public KeyCode right;
    public KeyCode left;

    public float distanceOnTheGround = 0.01f;

    public int coins = 0;

    public Vector3 point1;
    public Vector3 point2;
    public float radius;

    [Space]
    //Public variables for tracking
    public float angle;
    public float fractionBetweenAngles;
    public bool offTheGround;
    public bool currentlySliding;

    //Private variables
    private Rigidbody physicsBody;
    private DriftCompensation[] driftCompensations;
    private Rigidbody[] allRigidbodies;
    private CapsuleCollider collider;


    public float currentVelocity;
    public float velocityAfterAngleCorrection;
    private bool rightMove = false;
    private bool leftMove = false;
    private float dragOnGround;
    float thisRotation;

    public delegate void GameStateCheck(bool completed);
    public static event GameStateCheck GameWon;
    public static event GameStateCheck GameStarted;

    public Vector3 lastCheckpoint;
    public Vector3 lastCamPoint;

    void Start()
    {
        cameraObject = GameObject.FindGameObjectWithTag("MainCamera");
        lastCheckpoint = transform.parent.transform.position;
        lastCamPoint = cameraObject.transform.position;
        physicsBody = GetComponent<Rigidbody>();
        driftCompensations = GetComponentsInChildren<DriftCompensation>();
        allRigidbodies = GetComponentsInChildren<Rigidbody>();
        dragOnGround = physicsBody.drag;
    }

    void CheckForGround()
    {
        RaycastHit hit;
        Physics.CapsuleCast(transform.TransformPoint(point1), transform.TransformPoint(point2), radius, Vector3.down, out hit);
        if (hit.distance > distanceOnTheGround && !offTheGround)
        {
            OnFly();
        }
        else if (hit.distance < distanceOnTheGround && offTheGround)
        {
            OnLanding();
        }
    }

    void LockRotation()
    {
        //Calculate shortest rotation from down to forward
        //Quaternion q;
        //Vector3 v1 = Vector3.down;
        //Vector3 v2 = transform.forward;
        //Vector3 a = Vector3.Cross(v1, v2);
        //q = new Quaternion(a.x, a.y, a.z, Mathf.Sqrt((v1.magnitude * v1.magnitude) * (v2.magnitude * v2.magnitude)) + Vector3.Dot(v1, v2));
        //
        //float angle = Vector3.Angle(Vector3.down, transform.forward);
        //float difference = (180 - angleLock) - angle;
        //if (difference > 0)
        //{
        //    Debug.Log("hit");
        //    transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + Quaternion.S);
        //}

        Vector3 initialRotation = transform.rotation.eulerAngles;
        Vector3 fixedRotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, angleStiffness).eulerAngles;

        transform.rotation = Quaternion.Euler(fixedRotation.x, initialRotation.y, fixedRotation.z);

    }

    void FixedUpdate()
    {
        LockRotation();
        CheckForGround();
        if (!disableControl)
        {
            if (!offTheGround)
            {
                if (Input.GetKey(jump))
                {
                    if (!currentlySliding)
                    {
                        currentVelocity = accelerationOnJump;
                    }
                    currentlySliding = true;
                    leftMove = false;
                    rightMove = false;
                }
                else if (currentlySliding)
                {
                    Vector3 velUpDown = transform.up * Vector3.Dot(physicsBody.velocity, transform.up);
                    Vector3 velOther = physicsBody.velocity - velUpDown;
                    currentlySliding = false;
                    physicsBody.velocity = velOther + jumpVelocity;
                }

                //Calculate the angle in which the snake is bent atm
                Vector3 endTail = tailMiddle.position - lastTail.position;
                Vector3 startTail = physicsBody.position - tailMiddle.position;
                angle = Vector3.Angle(endTail, startTail);

                //Get the fraction of the current angle between straightAngle and FullStop Angle
                fractionBetweenAngles = 1 - Mathf.Clamp01((angle - straightAngle) / (fullStopAngle - straightAngle));

                if (Vector3.Dot(startTail - endTail, tailMiddle.transform.right) < 0)
                {
                    angle = -angle;
                }

                thisRotation = 0.0f;

                if (!currentlySliding)
                {
                    //Debug.Log("Not sliding");
                    if (Input.GetKey(right))
                    {
                        thisRotation = rotation;

                        rightMove = true;
                        leftMove = false;
                        currentVelocity = acceleration;
                    }
                    else if (Input.GetKey(left))
                    {
                        thisRotation = -rotation;

                        leftMove = true;
                        rightMove = false;
                        currentVelocity = acceleration;
                    }
                }
                else
                {
                    //Debug.Log("Sliding");
                    if (Input.GetKey(right))
                    {
                        thisRotation = rotationWhileSliding;
                        rightMove = true;
                    }
                    else if (Input.GetKey(left))
                    {
                        thisRotation = -rotationWhileSliding;
                        leftMove = true;
                    }
                }

                transform.Rotate(AxisOfRotation, thisRotation * physicsBody.velocity.magnitude * Time.fixedDeltaTime);

                velocityAfterAngleCorrection = currentVelocity;

                if ((rightMove == true && angle > 0) || (leftMove == true && angle < 0))
                {
                    velocityAfterAngleCorrection *= fractionBetweenAngles;
                }

                //Separate the up and down velocity and other velocity and rotate only the other velocity
                Vector3 velocityUpDown = transform.up * Vector3.Dot(physicsBody.velocity, transform.up);
                Vector3 velocityOther = physicsBody.velocity - velocityUpDown;

                if (!currentlySliding)
                {
                    velocityOther = transform.forward * Mathf.Min(maxVelocity, velocityOther.magnitude + velocityAfterAngleCorrection * Time.fixedDeltaTime);
                }
                else
                {
                    velocityOther = transform.forward * (velocityOther.magnitude + velocityAfterAngleCorrection * Time.fixedDeltaTime);
                }

                physicsBody.velocity = velocityOther + velocityUpDown;

            }
            else //if off the ground
            {
                currentlySliding = false;
            }
        }
        if(currentlySliding)
        {
            currentVelocity *= (Mathf.Max(0, 1 - (decelerationWhileSliding * Time.fixedDeltaTime)));
        }
        else
        {
            currentVelocity *= (Mathf.Max(0, 1 - (deceleration * Time.fixedDeltaTime)));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "WinCollider")
        {
            GameWon(true);
        }
        else if (other.tag == "CheckPoint")
        {
            lastCheckpoint = other.transform.position;
            lastCamPoint = cameraObject.transform.position;
            other.transform.parent.GetComponent<RuneStorage>().RunesOn();
            Destroy(other.gameObject);
        }
        else if (other.tag == "Coin")
        {
            other.transform.parent.GetComponent<AudioSource>().Play();
            Destroy(other.transform.parent.gameObject, 1.0f);
            Destroy(other.gameObject);
            coins++;
        }
        else
        {
            Destroy(other.gameObject);
            GameStarted(true);
            hitCollider = true;
        }
    }

    void OnLanding()
    {
        if (Input.GetKey(jump))
        {
            currentlySliding = true;
            leftMove = false;
            rightMove = false;
            currentVelocity = accelerationOnJump;

            //Separate the up and down velocity and other velocity and rotate only the other velocity
            Vector3 velocityUpDown = transform.up * Vector3.Dot(physicsBody.velocity, transform.up);
            Vector3 velocityOther = physicsBody.velocity - velocityUpDown;

            physicsBody.velocity = velocityOther.normalized * accelerationOnJump * 0.5f;
        }
        currentVelocity = acceleration;
        physicsBody.drag = dragOnGround;
        foreach (DriftCompensation i in driftCompensations)
        {
            i.enabled = true;
        }
        foreach (Rigidbody i in allRigidbodies)
        {
            i.constraints = RigidbodyConstraints.None;
        }
        offTheGround = false;
    }

    void OnFly()
    {
        physicsBody.drag = dragInAir;
        foreach (DriftCompensation i in driftCompensations)
        {
            i.enabled = false;
        }
        foreach (Rigidbody i in allRigidbodies)
        {
            i.constraints = RigidbodyConstraints.FreezeRotationZ;
        }
        offTheGround = true;
    }

    public void SpawnAtLastCheckpoint()
    {
        Destroy(gameObject);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.TransformPoint(point1), radius);
        Gizmos.DrawSphere(transform.TransformPoint(point2), radius);
    }
}
